[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / StepsLeaderboard

# Class: StepsLeaderboard

## Table of contents

### Constructors

- [constructor](StepsLeaderboard.md#constructor)

### Properties

- [\_id](StepsLeaderboard.md#_id)
- [ageGroup](StepsLeaderboard.md#agegroup)
- [ageGroupRank](StepsLeaderboard.md#agegrouprank)
- [totalStepCount](StepsLeaderboard.md#totalStepCount)
- [eventCategory](StepsLeaderboard.md#eventcategory)
- [eventId](StepsLeaderboard.md#eventid)
- [gender](StepsLeaderboard.md#gender)
- [genderRank](StepsLeaderboard.md#genderrank)
- [raceRank](StepsLeaderboard.md#racerank)
- [rank](StepsLeaderboard.md#rank)
- [updatedTime](StepsLeaderboard.md#updatedtime)
- [userId](StepsLeaderboard.md#userid)
- [userName](StepsLeaderboard.md#username)
- [profilePicture](StepsLeaderboard.md#profilepicture)
- [collectionName](StepsLeaderboard.md#collectionname)

## Constructors

### constructor

• **new StepsLeaderboard**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `Partial`<[`StepsLeaderboard`](StepsLeaderboard.md)\> |

#### Defined in

models/leaderboard/steps-leaderboard.ts:28

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:4
___

### ageGroup

• **ageGroup**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:11

___

### ageGroupRank

• **ageGroupRank**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:13

___

### totalStepCount

• **totalStepCount**: `number` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:8

___

### eventCategory

• **eventCategory**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:15

___

### eventId

• **eventId**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:14

___

### gender

• **gender**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:10

___

### genderRank

• **genderRank**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:12

___

### raceRank

• **raceRank**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:7

___

### rank

• **rank**: `number` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:6

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:18

___

### userId

• **userId**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:5

___

### userName

• **userName**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:9

___

### profilePicture

• **profilePicture**: `string` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:16

___
### previuosDayStepCount

• **previuosDayStepCount**: `number` = `null`

#### Defined in

models/leaderboard/steps-leaderboard.ts:17

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Event_StepsLeaderBoard"`

#### Defined in

models/leaderboard/steps-leaderboard.ts:39
