[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / Selfie

# Class: Selfie

Class representing Selfie collection
in mongoDB.

## Table of contents

### Constructors

- [constructor](Selfie.md#constructor)

### Properties

- [\_id](Selfie.md#_id)
- [activityId](Selfie.md#activityid)
- [createdTime](Selfie.md#createdtime)
- [data](Selfie.md#data)
- [fileName](Selfie.md#filename)
- [filePath](Selfie.md#filepath)
- [fileSize](Selfie.md#filesize)
- [location](Selfie.md#location)
- [source](Selfie.md#source)
- [updatedTime](Selfie.md#updatedtime)
- [url](Selfie.md#url)
- [userId](Selfie.md#userid)
- [collectionName](Selfie.md#collectionname)

## Constructors

### constructor

• **new Selfie**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `any` |

#### Defined in

models/selfie/selfie.ts:34

## Properties

### \_id

• **\_id**: `string` = `null`

Unique ID in mongo.
Must be a UUID.

#### Defined in

models/selfie/selfie.ts:18

___

### activityId

• **activityId**: `string` = `null`

#### Defined in

models/selfie/selfie.ts:24

___

### createdTime

• **createdTime**: `Date` = `null`

#### Defined in

models/selfie/selfie.ts:31

___

### data

• **data**: `string` = `null`

base64 string of binary data.

#### Defined in

models/selfie/selfie.ts:22

___

### fileName

• **fileName**: `string` = `null`

#### Defined in

models/selfie/selfie.ts:28

___

### filePath

• **filePath**: `string` = `null`

#### Defined in

models/selfie/selfie.ts:29

___

### fileSize

• **fileSize**: `number` = `null`

#### Defined in

models/selfie/selfie.ts:27

___

### location

• **location**: [`Geopoint`](Geopoint.md) = `null`

#### Defined in

models/selfie/selfie.ts:26

___

### source

• **source**: `string` = `null`

#### Defined in

models/selfie/selfie.ts:30

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/selfie/selfie.ts:32

___

### url

• **url**: `string` = `null`

#### Defined in

models/selfie/selfie.ts:23

___

### userId

• **userId**: `string` = `null`

#### Defined in

models/selfie/selfie.ts:25

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Activities_Selfie"`

#### Defined in

models/selfie/selfie.ts:60
