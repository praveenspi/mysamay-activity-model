[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / Geopoint

# Class: Geopoint

## Table of contents

### Constructors

- [constructor](Geopoint.md#constructor)

### Properties

- [\_id](Geopoint.md#_id)
- [accuracy](Geopoint.md#accuracy)
- [activityId](Geopoint.md#activityid)
- [alt](Geopoint.md#alt)
- [distance](Geopoint.md#distance)
- [distanceFromSteps](Geopoint.md#distancefromsteps)
- [duration](Geopoint.md#duration)
- [fluctuated](Geopoint.md#fluctuated)
- [heartRate](Geopoint.md#heartrate)
- [idle](Geopoint.md#idle)
- [lat](Geopoint.md#lat)
- [lon](Geopoint.md#lon)
- [pace](Geopoint.md#pace)
- [speed](Geopoint.md#speed)
- [timeStamp](Geopoint.md#timestamp)
- [type](Geopoint.md#type)
- [typeAccuracy](Geopoint.md#typeaccuracy)
- [collectionName](Geopoint.md#collectionname)

## Constructors

### constructor

• **new Geopoint**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `any` |

#### Defined in

models/activity/geopoint.ts:43

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/activity/geopoint.ts:4

___

### accuracy

• **accuracy**: `number` = `null`

GPS accuracy

#### Defined in

models/activity/geopoint.ts:23

___

### activityId

• **activityId**: `string` = `null`

#### Defined in

models/activity/geopoint.ts:5

___

### alt

• **alt**: `number` = `null`

Altitude

#### Defined in

models/activity/geopoint.ts:17

___

### distance

• **distance**: `number` = `null`

Distance covered since starting activity.

#### Defined in

models/activity/geopoint.ts:27

___

### distanceFromSteps

• **distanceFromSteps**: `number` = `null`

#### Defined in

models/activity/geopoint.ts:35

___

### duration

• **duration**: `number`

Seconds since starting activity.

#### Defined in

models/activity/geopoint.ts:40

___

### fluctuated

• **fluctuated**: `boolean` = `null`

#### Defined in

models/activity/geopoint.ts:18

___

### heartRate

• **heartRate**: `number` = `null`

#### Defined in

models/activity/geopoint.ts:34

___

### idle

• **idle**: `boolean` = `null`

#### Defined in

models/activity/geopoint.ts:19

___

### lat

• **lat**: `number` = `null`

Latitude

#### Defined in

models/activity/geopoint.ts:9

___

### lon

• **lon**: `number` = `null`

Longitude

#### Defined in

models/activity/geopoint.ts:13

___

### pace

• **pace**: `number` = `null`

#### Defined in

models/activity/geopoint.ts:32

___

### speed

• **speed**: `number` = `null`

#### Defined in

models/activity/geopoint.ts:33

___

### timeStamp

• **timeStamp**: `Date` = `null`

#### Defined in

models/activity/geopoint.ts:41

___

### type

• **type**: `string` = `null`

In vehicle, on foot etc.

#### Defined in

models/activity/geopoint.ts:31

___

### typeAccuracy

• **typeAccuracy**: `number` = `null`

#### Defined in

models/activity/geopoint.ts:36

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Activities_GeoLocations"`

#### Defined in

models/activity/geopoint.ts:62
