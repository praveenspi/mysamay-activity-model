[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / ActiveUser

# Class: ActiveUser

## Table of contents

### Constructors

- [constructor](ActiveUser.md#constructor)

### Properties

- [\_id](ActiveUser.md#_id)
- [activityCount](ActiveUser.md#activitycount)
- [avgDistance](ActiveUser.md#avgdistance)
- [avgPace](ActiveUser.md#avgpace)
- [date](ActiveUser.md#date)
- [day](ActiveUser.md#day)
- [gender](ActiveUser.md#gender)
- [month](ActiveUser.md#month)
- [previousDistance](ActiveUser.md#previousdistance)
- [totalDistance](ActiveUser.md#totaldistance)
- [totalTime](ActiveUser.md#totaltime)
- [updatedTime](ActiveUser.md#updatedtime)
- [userId](ActiveUser.md#userid)
- [userName](ActiveUser.md#username)
- [year](ActiveUser.md#year)
- [collectionName](ActiveUser.md#collectionname)

## Constructors

### constructor

• **new ActiveUser**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `any` |

#### Defined in

models/reports/active-user.ts:21

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/reports/active-user.ts:4

___

### activityCount

• **activityCount**: `number` = `null`

#### Defined in

models/reports/active-user.ts:11

___

### avgDistance

• **avgDistance**: `number` = `null`

#### Defined in

models/reports/active-user.ts:16

___

### avgPace

• **avgPace**: `number` = `null`

#### Defined in

models/reports/active-user.ts:15

___

### date

• **date**: `Date` = `null`

#### Defined in

models/reports/active-user.ts:17

___

### day

• **day**: `number` = `null`

#### Defined in

models/reports/active-user.ts:8

___

### gender

• **gender**: `string` = `null`

#### Defined in

models/reports/active-user.ts:7

___

### month

• **month**: `number` = `null`

#### Defined in

models/reports/active-user.ts:9

___

### previousDistance

• **previousDistance**: `number` = `null`

#### Defined in

models/reports/active-user.ts:12

___

### totalDistance

• **totalDistance**: `number` = `null`

#### Defined in

models/reports/active-user.ts:13

___

### totalTime

• **totalTime**: `number` = `null`

#### Defined in

models/reports/active-user.ts:14

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/reports/active-user.ts:18

___

### userId

• **userId**: `string` = `null`

#### Defined in

models/reports/active-user.ts:5

___

### userName

• **userName**: `string` = `null`

#### Defined in

models/reports/active-user.ts:6

___

### year

• **year**: `number` = `null`

#### Defined in

models/reports/active-user.ts:10

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Reports_ActiveUsers"`

#### Defined in

models/reports/active-user.ts:41
