[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / DailyStepCount

# Class: DailyStepCount

## Table of contents

### Constructors

- [constructor](DailyStepCount.md#constructor)

### Properties

- [userId](DailyStepCount.md#userId)
- [userName](DailyStepCount.md#userName)
- [gender](DailyStepCount.md#gender)
- [day](DailyStepCount.md#day)
- [month](DailyStepCount.md#month)
- [year](DailyStepCount.md#year)
- [stepCount](DailyStepCount.md#stepCount)
- [eventId](DailyStepCount.md#eventid)
- [createdTime](DailyStepCount.md#createdTime)
- [updatedTime](DailyStepCount.md#updatedtime)
- [collectionName](DailyStepCount.md#collectionname)

## Constructors

### constructor

• **new DailyStepCount**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `Partial`<[`DailyStepCount`](DailyStepCount.md)\> |

#### Defined in

models/daily-step-count/daily-step-count.ts:15

## Properties

### userId

• **userId**: `number` = `null`

#### Defined in

models/daily-step-count/daily-step-count.ts:5

___

### userName

• **userName**: `string` = `null`

#### Defined in

models/daily-step-count/daily-step-count.ts:6

___

### gender

• **gender**: `string` = `null`

#### Defined in

models/daily-step-count/daily-step-count.ts:7

___

### day

• **day**: `number` = `null`

#### Defined in

models/daily-step-count/daily-step-count.ts:8

___

### month

• **month**: `number` = `null`

#### Defined in

models/daily-step-count/daily-step-count.ts:9

___

### year

• **year**: `number` = `null`

#### Defined in

models/daily-step-count/daily-step-count.ts:10

___

### stepCount

• **stepCount**: `number` = `null`

#### Defined in

models/daily-step-count/daily-step-count.ts:11

___

### eventId

• **eventId**: `string` = `null`

#### Defined in

models/daily-step-count/daily-step-count.ts:12

___
### date

• **date**: `Date` = `null`

#### Defined in

models/daily-step-count/daily-step-count.ts:13

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/daily-step-count/daily-step-count.ts:14

___

### collectionName

▪ `Static` **collectionName**: `string` = `"DailyStepCount"`

#### Defined in

models/daily-step-count/daily-step-count.ts:37
