[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / MonthlyActiveUser

# Class: MonthlyActiveUser

## Table of contents

### Constructors

- [constructor](MonthlyActiveUser.md#constructor)

### Properties

- [\_id](MonthlyActiveUser.md#_id)
- [activityCount](MonthlyActiveUser.md#activitycount)
- [avgDistance](MonthlyActiveUser.md#avgdistance)
- [avgPace](MonthlyActiveUser.md#avgpace)
- [date](MonthlyActiveUser.md#date)
- [gender](MonthlyActiveUser.md#gender)
- [month](MonthlyActiveUser.md#month)
- [totalDistance](MonthlyActiveUser.md#totaldistance)
- [totalTime](MonthlyActiveUser.md#totaltime)
- [updatedTime](MonthlyActiveUser.md#updatedtime)
- [userId](MonthlyActiveUser.md#userid)
- [userName](MonthlyActiveUser.md#username)
- [year](MonthlyActiveUser.md#year)
- [collectionName](MonthlyActiveUser.md#collectionname)

## Constructors

### constructor

• **new MonthlyActiveUser**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `any` |

#### Defined in

models/reports/monthly-active-user.ts:19

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/reports/monthly-active-user.ts:4

___

### activityCount

• **activityCount**: `number` = `null`

#### Defined in

models/reports/monthly-active-user.ts:10

___

### avgDistance

• **avgDistance**: `number` = `null`

#### Defined in

models/reports/monthly-active-user.ts:14

___

### avgPace

• **avgPace**: `number` = `null`

#### Defined in

models/reports/monthly-active-user.ts:13

___

### date

• **date**: `Date` = `null`

#### Defined in

models/reports/monthly-active-user.ts:15

___

### gender

• **gender**: `string` = `null`

#### Defined in

models/reports/monthly-active-user.ts:7

___

### month

• **month**: `number` = `null`

#### Defined in

models/reports/monthly-active-user.ts:8

___

### totalDistance

• **totalDistance**: `number` = `null`

#### Defined in

models/reports/monthly-active-user.ts:11

___

### totalTime

• **totalTime**: `number` = `null`

#### Defined in

models/reports/monthly-active-user.ts:12

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/reports/monthly-active-user.ts:16

___

### userId

• **userId**: `string` = `null`

#### Defined in

models/reports/monthly-active-user.ts:5

___

### userName

• **userName**: `string` = `null`

#### Defined in

models/reports/monthly-active-user.ts:6

___

### year

• **year**: `number` = `null`

#### Defined in

models/reports/monthly-active-user.ts:9

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Reports_MonthlyActiveUsers"`

#### Defined in

models/reports/monthly-active-user.ts:39
