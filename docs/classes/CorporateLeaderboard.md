[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / CorporateLeaderboard

# Class: CorporateLeaderboard

## Table of contents

### Constructors

- [constructor](CorporateLeaderboard.md#constructor)

### Properties

- [\_id](CorporateLeaderboard.md#_id)
- [activityCount](CorporateLeaderboard.md#activitycount)
- [corporateName](CorporateLeaderboard.md#corporatename)
- [eventId](CorporateLeaderboard.md#eventid)
- [groupId](CorporateLeaderboard.md#groupid)
- [rank](CorporateLeaderboard.md#rank)
- [totalDistance](CorporateLeaderboard.md#totaldistance)
- [totalParticipants](CorporateLeaderboard.md#totalparticipants)
- [updatedTime](CorporateLeaderboard.md#updatedtime)
- [collectionName](CorporateLeaderboard.md#collectionname)

## Constructors

### constructor

• **new CorporateLeaderboard**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `Partial`<[`CorporateLeaderboard`](CorporateLeaderboard.md)\> |

#### Defined in

models/leaderboard/corporate-leaderboard.ts:14

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/leaderboard/corporate-leaderboard.ts:4

___

### activityCount

• **activityCount**: `number` = `null`

#### Defined in

models/leaderboard/corporate-leaderboard.ts:8

___

### corporateName

• **corporateName**: `string` = `null`

#### Defined in

models/leaderboard/corporate-leaderboard.ts:6

___

### eventId

• **eventId**: `string` = `null`

#### Defined in

models/leaderboard/corporate-leaderboard.ts:11

___

### groupId

• **groupId**: `string` = `null`

#### Defined in

models/leaderboard/corporate-leaderboard.ts:5

___

### rank

• **rank**: `number` = `null`

#### Defined in

models/leaderboard/corporate-leaderboard.ts:7

___

### totalDistance

• **totalDistance**: `number` = `null`

#### Defined in

models/leaderboard/corporate-leaderboard.ts:9

___

### totalParticipants

• **totalParticipants**: `number` = `null`

#### Defined in

models/leaderboard/corporate-leaderboard.ts:10

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/leaderboard/corporate-leaderboard.ts:12

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Events_CorporateLeaderboard"`

#### Defined in

models/leaderboard/corporate-leaderboard.ts:33
