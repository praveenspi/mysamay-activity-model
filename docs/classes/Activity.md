[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / Activity

# Class: Activity

## Table of contents

### Constructors

- [constructor](Activity.md#constructor)

### Properties

- [\_id](Activity.md#_id)
- [activeTime](Activity.md#activetime)
- [activityDescription](Activity.md#activitydescription)
- [activityMode](Activity.md#activitymode)
- [activitySource](Activity.md#activitysource)
- [activityStatus](Activity.md#activitystatus)
- [activityStatusReason](Activity.md#activitystatusreason)
- [activityTitle](Activity.md#activitytitle)
- [activityType](Activity.md#activitytype)
- [avgCadence](Activity.md#avgcadence)
- [avgHeartRate](Activity.md#avgheartrate)
- [avgPace](Activity.md#avgpace)
- [avgSpeed](Activity.md#avgspeed)
- [calories](Activity.md#calories)
- [cheatingDetected](Activity.md#cheatingdetected)
- [createdTime](Activity.md#createdtime)
- [date](Activity.md#date)
- [detectedActvityType](Activity.md#detectedactvitytype)
- [deviceInfo](Activity.md#deviceinfo)
- [distance](Activity.md#distance)
- [elapsedTime](Activity.md#elapsedtime)
- [elevationGained](Activity.md#elevationgained)
- [elevationLost](Activity.md#elevationlost)
- [endTime](Activity.md#endtime)
- [eventCategory](Activity.md#eventcategory)
- [eventId](Activity.md#eventid)
- [eventLogo](Activity.md#eventlogo)
- [eventName](Activity.md#eventname)
- [externalActivityId](Activity.md#externalactivityid)
- [imported](Activity.md#imported)
- [initialSyncDate](Activity.md#initialsyncdate)
- [manual](Activity.md#manual)
- [polyline](Activity.md#polyline)
- [polylineStr](Activity.md#polylinestr)
- [selfieCount](Activity.md#selfiecount)
- [sourceName](Activity.md#sourcename)
- [startTime](Activity.md#starttime)
- [stepCount](Activity.md#stepcount)
- [thumbnail](Activity.md#thumbnail)
- [updatedTime](Activity.md#updatedtime)
- [userId](Activity.md#userid)
- [collectionName](Activity.md#collectionname)

## Constructors

### constructor

• **new Activity**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `Partial`<[`Activity`](Activity.md)\> |

#### Defined in

models/activity/activity.ts:81

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/activity/activity.ts:14

___

### activeTime

• **activeTime**: `number` = `null`

Moving time in seconds.

#### Defined in

models/activity/activity.ts:50

___

### activityDescription

• **activityDescription**: `string` = `null`

#### Defined in

models/activity/activity.ts:31

___

### activityMode

• **activityMode**: `string` = `null`

In vehicle, on foot etc.

#### Defined in

models/activity/activity.ts:35

___

### activitySource

• **activitySource**: `string` = `"MySamay"`

#### Defined in

models/activity/activity.ts:73

___

### activityStatus

• **activityStatus**: `string` = `null`

Approved or Rejected.

#### Defined in

models/activity/activity.ts:25

___

### activityStatusReason

• **activityStatusReason**: `string` = `null`

Reason for Rejection.

#### Defined in

models/activity/activity.ts:29

___

### activityTitle

• **activityTitle**: `string` = `null`

#### Defined in

models/activity/activity.ts:21

___

### activityType

• **activityType**: `string` = `null`

Type of activity.
Run | Walk

#### Defined in

models/activity/activity.ts:19

___

### avgCadence

• **avgCadence**: `number` = `null`

#### Defined in

models/activity/activity.ts:54

___

### avgHeartRate

• **avgHeartRate**: `number` = `null`

#### Defined in

models/activity/activity.ts:55

___

### avgPace

• **avgPace**: `number` = `null`

#### Defined in

models/activity/activity.ts:52

___

### avgSpeed

• **avgSpeed**: `number` = `null`

#### Defined in

models/activity/activity.ts:53

___

### calories

• **calories**: `number` = `null`

#### Defined in

models/activity/activity.ts:56

___

### cheatingDetected

• **cheatingDetected**: `boolean` = `false`

#### Defined in

models/activity/activity.ts:30

___

### createdTime

• **createdTime**: `Date` = `null`

#### Defined in

models/activity/activity.ts:70

___

### date

• **date**: `string` = `null`

#### Defined in

models/activity/activity.ts:72

___

### detectedActvityType

• **detectedActvityType**: `string` = `null`

#### Defined in

models/activity/activity.ts:20

___

### deviceInfo

• **deviceInfo**: `DeviceInfo` = `null`

#### Defined in

models/activity/activity.ts:69

___

### distance

• **distance**: `number` = `null`

Distance covered in KM

#### Defined in

models/activity/activity.ts:42

___

### elapsedTime

• **elapsedTime**: `number` = `null`

Total time in seconds.

#### Defined in

models/activity/activity.ts:46

___

### elevationGained

• **elevationGained**: `number` = `null`

#### Defined in

models/activity/activity.ts:57

___

### elevationLost

• **elevationLost**: `number` = `null`

#### Defined in

models/activity/activity.ts:58

___

### endTime

• **endTime**: `Date`

#### Defined in

models/activity/activity.ts:38

___

### eventCategory

• **eventCategory**: `string` = `null`

#### Defined in

models/activity/activity.ts:63

___

### eventId

• **eventId**: `string` = `null`

#### Defined in

models/activity/activity.ts:61

___

### eventLogo

• **eventLogo**: `string` = `null`

#### Defined in

models/activity/activity.ts:64

___

### eventName

• **eventName**: `string` = `null`

#### Defined in

models/activity/activity.ts:62

___

### externalActivityId

• **externalActivityId**: `string` = `null`

#### Defined in

models/activity/activity.ts:74

___

### imported

• **imported**: `boolean` = `false`

#### Defined in

models/activity/activity.ts:68

___

### initialSyncDate

• **initialSyncDate**: `Date` = `null`

#### Defined in

models/activity/activity.ts:79

___

### manual

• **manual**: `boolean` = `false`

#### Defined in

models/activity/activity.ts:67

___

### polyline

• **polyline**: [`Geopoint`](Geopoint.md)[] = `null`

#### Defined in

models/activity/activity.ts:59

___

### polylineStr

• **polylineStr**: `string` = `null`

#### Defined in

models/activity/activity.ts:60

___

### selfieCount

• **selfieCount**: `number` = `null`

#### Defined in

models/activity/activity.ts:65

___

### sourceName

• **sourceName**: `string` = `null`

Name of the device on which activity was recorded.

#### Defined in

models/activity/activity.ts:78

___

### startTime

• **startTime**: `Date`

#### Defined in

models/activity/activity.ts:37

___

### stepCount

• **stepCount**: `number` = `null`

#### Defined in

models/activity/activity.ts:51

___

### thumbnail

• **thumbnail**: `string` = `null`

#### Defined in

models/activity/activity.ts:66

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/activity/activity.ts:71

___

### userId

• **userId**: `string` = `null`

#### Defined in

models/activity/activity.ts:36

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Activities_Activity"`

#### Defined in

models/activity/activity.ts:109
