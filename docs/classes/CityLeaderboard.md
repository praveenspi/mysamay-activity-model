[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / CityLeaderboard

# Class: CityLeaderboard

## Table of contents

### Constructors

- [constructor](CityLeaderboard.md#constructor)

### Properties

- [\_id](CityLeaderboard.md#_id)
- [activityCount](CityLeaderboard.md#activitycount)
- [city](CityLeaderboard.md#city)
- [eventId](CityLeaderboard.md#eventid)
- [rank](CityLeaderboard.md#rank)
- [totalDistance](CityLeaderboard.md#totaldistance)
- [totalParticipants](CityLeaderboard.md#totalparticipants)
- [updatedTime](CityLeaderboard.md#updatedtime)
- [collectionName](CityLeaderboard.md#collectionname)

## Constructors

### constructor

• **new CityLeaderboard**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `Partial`<[`CityLeaderboard`](CityLeaderboard.md)\> |

#### Defined in

models/leaderboard/city-leaderboard.ts:13

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/leaderboard/city-leaderboard.ts:4

___

### activityCount

• **activityCount**: `number` = `null`

#### Defined in

models/leaderboard/city-leaderboard.ts:7

___

### city

• **city**: `string` = `null`

#### Defined in

models/leaderboard/city-leaderboard.ts:5

___

### eventId

• **eventId**: `string` = `null`

#### Defined in

models/leaderboard/city-leaderboard.ts:10

___

### rank

• **rank**: `number` = `null`

#### Defined in

models/leaderboard/city-leaderboard.ts:6

___

### totalDistance

• **totalDistance**: `number` = `null`

#### Defined in

models/leaderboard/city-leaderboard.ts:8

___

### totalParticipants

• **totalParticipants**: `number` = `null`

#### Defined in

models/leaderboard/city-leaderboard.ts:9

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/leaderboard/city-leaderboard.ts:11

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Events_CityLeaderboard"`

#### Defined in

models/leaderboard/city-leaderboard.ts:32
