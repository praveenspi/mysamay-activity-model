[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / Splits

# Class: Splits

Class representing Selfie collection
in mongoDB.

## Table of contents

### Constructors

- [constructor](Splits.md#constructor)

### Properties

- [\_id](Splits.md#_id)
- [activityId](Splits.md#activityid)
- [createdTime](Splits.md#createdtime)
- [distance](Splits.md#distance)
- [distanceOrder](Splits.md#distanceorder)
- [timeTakenInSecs](Splits.md#timetakeninsecs)
- [updatedTime](Splits.md#updatedtime)
- [userId](Splits.md#userid)
- [collectionName](Splits.md#collectionname)

## Constructors

### constructor

• **new Splits**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `any` |

#### Defined in

models/splits/splits.ts:22

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/splits/splits.ts:13

___

### activityId

• **activityId**: `string` = `null`

#### Defined in

models/splits/splits.ts:14

___

### createdTime

• **createdTime**: `Date` = `null`

#### Defined in

models/splits/splits.ts:19

___

### distance

• **distance**: `number` = `null`

#### Defined in

models/splits/splits.ts:17

___

### distanceOrder

• **distanceOrder**: `number` = `null`

#### Defined in

models/splits/splits.ts:16

___

### timeTakenInSecs

• **timeTakenInSecs**: `number` = `null`

#### Defined in

models/splits/splits.ts:18

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/splits/splits.ts:20

___

### userId

• **userId**: `string` = `null`

#### Defined in

models/splits/splits.ts:15

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Activities_Splits"`

#### Defined in

models/splits/splits.ts:45
