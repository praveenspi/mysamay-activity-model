[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / DailyDistance

# Class: DailyDistance

## Table of contents

### Constructors

- [constructor](DailyDistance.md#constructor)

### Properties

- [\_id](DailyDistance.md#_id)
- [activityCount](DailyDistance.md#activitycount)
- [date](DailyDistance.md#date)
- [distance](DailyDistance.md#distance)
- [eventId](DailyDistance.md#eventid)
- [groupId](DailyDistance.md#groupid)
- [updatedTime](DailyDistance.md#updatedtime)
- [collectionName](DailyDistance.md#collectionname)

## Constructors

### constructor

• **new DailyDistance**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `Partial`<[`DailyDistance`](DailyDistance.md)\> |

#### Defined in

models/daily-limits/daily-distance.ts:17

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/daily-limits/daily-distance.ts:9

___

### activityCount

• **activityCount**: `number` = `null`

#### Defined in

models/daily-limits/daily-distance.ts:12

___

### date

• **date**: `Date` = `null`

#### Defined in

models/daily-limits/daily-distance.ts:14

___

### distance

• **distance**: `number` = `null`

#### Defined in

models/daily-limits/daily-distance.ts:11

___

### eventId

• **eventId**: `string` = `null`

#### Defined in

models/daily-limits/daily-distance.ts:13

___

### groupId

• **groupId**: `string` = `null`

#### Defined in

models/daily-limits/daily-distance.ts:10

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/daily-limits/daily-distance.ts:15

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Events_DailyDistance"`

#### Defined in

models/daily-limits/daily-distance.ts:36
