[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / Pace

# Class: Pace

## Table of contents

### Constructors

- [constructor](Pace.md#constructor)

### Properties

- [\_id](Pace.md#_id)
- [activityId](Pace.md#activityid)
- [distance](Pace.md#distance)
- [distanceFromSteps](Geopoint.md#distancefromsteps)
- [pace](Pace.md#pace)
- [timeStamp](Pace.md#timestamp)
- [collectionName](Pace.md#collectionname)

## Constructors

### constructor

• **new Pace**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `any` |

#### Defined in

models/pace/pace.ts:11

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/pace/pace.ts:4

___

### activityId

• **activityId**: `string` = `null`

#### Defined in

models/pace/pace.ts:5

___

### distance

• **distance**: `number` = `null`

Distance covered since starting activity.

#### Defined in

models/pace/pace.ts:6

___

### distanceFromSteps

• **distanceFromSteps**: `number` = `null`

#### Defined in

models/activity/geopoint.ts:7

___
### pace

• **pace**: `number` = `null`

#### Defined in

models/pace/pace.ts:8

___

### timeStamp

• **timeStamp**: `Date` = `null`

#### Defined in

models/pace/pace.ts:9

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Activities_Pace"`

#### Defined in

models/pace/pace.ts:30
