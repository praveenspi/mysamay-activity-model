[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / ActivityLeaderboard

# Class: ActivityLeaderboard

## Table of contents

### Constructors

- [constructor](ActivityLeaderboard.md#constructor)

### Properties

- [\_id](ActivityLeaderboard.md#_id)
- [activityCount](ActivityLeaderboard.md#activitycount)
- [avgDistance](ActivityLeaderboard.md#avgdistance)
- [avgPace](ActivityLeaderboard.md#avgpace)
- [gender](ActivityLeaderboard.md#gender)
- [totalDistance](ActivityLeaderboard.md#totaldistance)
- [type](ActivityLeaderboard.md#type)
- [updatedTime](ActivityLeaderboard.md#updatedtime)
- [userId](ActivityLeaderboard.md#userid)
- [userName](ActivityLeaderboard.md#username)
- [collectionName](ActivityLeaderboard.md#collectionname)

## Constructors

### constructor

• **new ActivityLeaderboard**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `Partial`<[`ActivityLeaderboard`](ActivityLeaderboard.md)\> |

#### Defined in

models/leaderboard/activity-leaderboard.ts:16

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/leaderboard/activity-leaderboard.ts:4

___

### activityCount

• **activityCount**: `number` = `null`

#### Defined in

models/leaderboard/activity-leaderboard.ts:6

___

### avgDistance

• **avgDistance**: `number` = `null`

#### Defined in

models/leaderboard/activity-leaderboard.ts:8

___

### avgPace

• **avgPace**: `number` = `null`

#### Defined in

models/leaderboard/activity-leaderboard.ts:9

___

### gender

• **gender**: `string` = `null`

#### Defined in

models/leaderboard/activity-leaderboard.ts:12

___

### totalDistance

• **totalDistance**: `number` = `null`

#### Defined in

models/leaderboard/activity-leaderboard.ts:7

___

### type

• **type**: `string` = `null`

#### Defined in

models/leaderboard/activity-leaderboard.ts:10

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/leaderboard/activity-leaderboard.ts:13

___

### userId

• **userId**: `string` = `null`

#### Defined in

models/leaderboard/activity-leaderboard.ts:5

___

### userName

• **userName**: `string` = `null`

#### Defined in

models/leaderboard/activity-leaderboard.ts:11

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Activities_LeaderBoard"`

#### Defined in

models/leaderboard/activity-leaderboard.ts:35
