[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / EventLeaderboard

# Class: EventLeaderboard

## Table of contents

### Constructors

- [constructor](EventLeaderboard.md#constructor)

### Properties

- [\_id](EventLeaderboard.md#_id)
- [activeTime](EventLeaderboard.md#activetime)
- [activityCount](EventLeaderboard.md#activitycount)
- [actualDistance](EventLeaderboard.md#actualdistance)
- [ageGroup](EventLeaderboard.md#agegroup)
- [ageGroupRank](EventLeaderboard.md#agegrouprank)
- [avgDistance](EventLeaderboard.md#avgdistance)
- [avgPace](EventLeaderboard.md#avgpace)
- [clubId](EventLeaderboard.md#clubid)
- [consideredTime](EventLeaderboard.md#consideredtime)
- [corporateId](EventLeaderboard.md#corporateid)
- [eventCategory](EventLeaderboard.md#eventcategory)
- [eventId](EventLeaderboard.md#eventid)
- [gender](EventLeaderboard.md#gender)
- [genderRank](EventLeaderboard.md#genderrank)
- [raceRank](EventLeaderboard.md#racerank)
- [rank](EventLeaderboard.md#rank)
- [totalDistance](EventLeaderboard.md#totaldistance)
- [type](EventLeaderboard.md#type)
- [updatedTime](EventLeaderboard.md#updatedtime)
- [userId](EventLeaderboard.md#userid)
- [userName](EventLeaderboard.md#username)
- [collectionName](EventLeaderboard.md#collectionname)

## Constructors

### constructor

• **new EventLeaderboard**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `Partial`<[`EventLeaderboard`](EventLeaderboard.md)\> |

#### Defined in

models/leaderboard/event-leaderboard.ts:28

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:4

___

### activeTime

• **activeTime**: `number` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:19

___

### activityCount

• **activityCount**: `number` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:8

___

### actualDistance

• **actualDistance**: `number` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:10

___

### ageGroup

• **ageGroup**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:16

___

### ageGroupRank

• **ageGroupRank**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:18

___

### avgDistance

• **avgDistance**: `number` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:11

___

### avgPace

• **avgPace**: `number` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:12

___

### clubId

• **clubId**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:21

___

### consideredTime

• **consideredTime**: `number` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:20

___

### corporateId

• **corporateId**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:22

___

### eventCategory

• **eventCategory**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:24

___

### eventId

• **eventId**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:23

___

### gender

• **gender**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:15

___

### genderRank

• **genderRank**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:17

___

### raceRank

• **raceRank**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:7

___

### rank

• **rank**: `number` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:6

___

### totalDistance

• **totalDistance**: `number` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:9

___

### type

• **type**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:13

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:25

___

### userId

• **userId**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:5

___

### userName

• **userName**: `string` = `null`

#### Defined in

models/leaderboard/event-leaderboard.ts:14

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Events_LeaderBoard"`

#### Defined in

models/leaderboard/event-leaderboard.ts:47
