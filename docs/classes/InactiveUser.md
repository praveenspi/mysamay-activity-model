[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / InactiveUser

# Class: InactiveUser

## Table of contents

### Constructors

- [constructor](InactiveUser.md#constructor)

### Properties

- [\_id](InactiveUser.md#_id)
- [date](InactiveUser.md#date)
- [day](InactiveUser.md#day)
- [email](InactiveUser.md#email)
- [gender](InactiveUser.md#gender)
- [lastActiveTime](InactiveUser.md#lastactivetime)
- [mobile](InactiveUser.md#mobile)
- [month](InactiveUser.md#month)
- [updatedTime](InactiveUser.md#updatedtime)
- [userId](InactiveUser.md#userid)
- [userName](InactiveUser.md#username)
- [year](InactiveUser.md#year)
- [collectionName](InactiveUser.md#collectionname)

## Constructors

### constructor

• **new InactiveUser**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `any` |

#### Defined in

models/reports/inactive-users.ts:18

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/reports/inactive-users.ts:4

___

### date

• **date**: `Date` = `null`

#### Defined in

models/reports/inactive-users.ts:13

___

### day

• **day**: `number` = `null`

#### Defined in

models/reports/inactive-users.ts:10

___

### email

• **email**: `string` = `null`

#### Defined in

models/reports/inactive-users.ts:7

___

### gender

• **gender**: `string` = `null`

#### Defined in

models/reports/inactive-users.ts:9

___

### lastActiveTime

• **lastActiveTime**: `Date` = `null`

#### Defined in

models/reports/inactive-users.ts:15

___

### mobile

• **mobile**: `string` = `null`

#### Defined in

models/reports/inactive-users.ts:8

___

### month

• **month**: `number` = `null`

#### Defined in

models/reports/inactive-users.ts:11

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/reports/inactive-users.ts:14

___

### userId

• **userId**: `string` = `null`

#### Defined in

models/reports/inactive-users.ts:5

___

### userName

• **userName**: `string` = `null`

#### Defined in

models/reports/inactive-users.ts:6

___

### year

• **year**: `number` = `null`

#### Defined in

models/reports/inactive-users.ts:12

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Reports_InactiveUsers"`

#### Defined in

models/reports/inactive-users.ts:38
