[@mysamay/mysamay-activity-model](../README.md) / [Exports](../modules.md) / ClubLeaderboard

# Class: ClubLeaderboard

## Table of contents

### Constructors

- [constructor](ClubLeaderboard.md#constructor)

### Properties

- [\_id](ClubLeaderboard.md#_id)
- [activityCount](ClubLeaderboard.md#activitycount)
- [clubName](ClubLeaderboard.md#clubname)
- [eventId](ClubLeaderboard.md#eventid)
- [groupId](ClubLeaderboard.md#groupid)
- [rank](ClubLeaderboard.md#rank)
- [totalDistance](ClubLeaderboard.md#totaldistance)
- [totalParticipants](ClubLeaderboard.md#totalparticipants)
- [updatedTime](ClubLeaderboard.md#updatedtime)
- [collectionName](ClubLeaderboard.md#collectionname)

## Constructors

### constructor

• **new ClubLeaderboard**(`data`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `Partial`<[`ClubLeaderboard`](ClubLeaderboard.md)\> |

#### Defined in

models/leaderboard/club-leaderboard.ts:14

## Properties

### \_id

• **\_id**: `string` = `null`

#### Defined in

models/leaderboard/club-leaderboard.ts:4

___

### activityCount

• **activityCount**: `number` = `null`

#### Defined in

models/leaderboard/club-leaderboard.ts:8

___

### clubName

• **clubName**: `string` = `null`

#### Defined in

models/leaderboard/club-leaderboard.ts:6

___

### eventId

• **eventId**: `string` = `null`

#### Defined in

models/leaderboard/club-leaderboard.ts:11

___

### groupId

• **groupId**: `string` = `null`

#### Defined in

models/leaderboard/club-leaderboard.ts:5

___

### rank

• **rank**: `number` = `null`

#### Defined in

models/leaderboard/club-leaderboard.ts:7

___

### totalDistance

• **totalDistance**: `number` = `null`

#### Defined in

models/leaderboard/club-leaderboard.ts:9

___

### totalParticipants

• **totalParticipants**: `number` = `null`

#### Defined in

models/leaderboard/club-leaderboard.ts:10

___

### updatedTime

• **updatedTime**: `Date` = `null`

#### Defined in

models/leaderboard/club-leaderboard.ts:12

___

### collectionName

▪ `Static` **collectionName**: `string` = `"Events_ClubLeaderboard"`

#### Defined in

models/leaderboard/club-leaderboard.ts:33
