import { MetadataHelper } from '@praveenspi/mysamay-common-util';
export class ExternalActivity {
    _id: string = null;
    source: string = null;
    processed: boolean = false;
    syncDate: Date = null;
    eventType: string = null;
    discardedReason: string = null;
    rateLimit: string = null;
    usage: string = null;
    activityId: string = null;
    userId: string = null;
    data: any = null;

    constructor(data: Partial<ExternalActivity>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                this[key] = data[key];
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Activity_ExternalActivities";
}