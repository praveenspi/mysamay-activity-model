import { MetadataHelper } from "@praveenspi/mysamay-common-util";

export class DailyStepCount {
    _id: string = null;
    userId: string = null;
    userName: string = null;
    gender: string = null;
    day: number = null;
    month: number = null;
    year: number = null;
    stepCount: number = null;
    eventId: string = null;
    date: Date = null;
    source: string = null;
    updatedTime: Date = null;

    constructor(data: Partial<DailyStepCount>) {
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (key === "updatedTime") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        this.updatedTime = new Date();
    }

    static collectionName = "DailyStepCount";
}