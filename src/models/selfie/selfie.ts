import { MetadataHelper } from "@praveenspi/mysamay-common-util";
import { Geopoint } from "../activity/geopoint";

const dateFields = [
    "createdTime",
    "updatedTime"
];

/**
 * Class representing Selfie collection 
 * in mongoDB.
 */
export class Selfie {
    /**
     * Unique ID in mongo.
     * Must be a UUID.
     */
    _id: string = null;
    /**
     * base64 string of binary data.
     */
    data: string = null;
    url: string = null;
    activityId: string = null;
    userId: string = null;
    location: Geopoint = null;
    fileSize: number = null;
    fileName: string = null;
    filePath: string = null;
    source: string = null;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(key === "location") {
                    this[key] = new Geopoint(data[key]);
                }
                else if(dateFields.find(dateField => dateField === key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if (!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Activities_Selfie";
}