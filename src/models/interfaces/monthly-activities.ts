export interface MonthlyActivities {
    month: number;
    activityCount: number;
}