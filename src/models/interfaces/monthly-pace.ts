export interface MonthlyPace {
    month: number;
    avgPace: number;
}