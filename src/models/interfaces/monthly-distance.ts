export interface MonthlyDistance {
    month: number;
    distance: number;
}