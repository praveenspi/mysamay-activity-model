import { MetadataHelper } from "@praveenspi/mysamay-common-util";

const dateFields = [
    "createdTime",
    "updatedTime"
];

/**
 * Class representing Photo Frames collection 
 * in mongoDB.
 */
export class PhotoFrame {
    /**
     * Unique ID in mongo.
     * Must be a UUID.
     */
    _id: string = null;
    /**
     * AWS S3 public URL.
     */
    url: string = null;
    events: string[] = null;
    name: string = null;
    description: string = null;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(dateFields.find(dateField => dateField === key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if (!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Files_PhotoFrames";
}