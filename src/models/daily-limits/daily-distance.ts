import { MetadataHelper } from '@praveenspi/mysamay-common-util';


const dateFields = [
    "date",
    "updatedTime"
]
export class DailyDistance {
    _id: string = null;
    groupId: string = null;
    userId: string = null;
    distance: number = null;
    activityCount: number = null;
    eventId: string = null;
    date: Date = null;
    updatedTime: Date = null;

    constructor(data: Partial<DailyDistance>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (dateFields.find(k => k === key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Events_DailyDistance";
}