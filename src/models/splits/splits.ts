import { MetadataHelper } from "@praveenspi/mysamay-common-util";

const dateFields = [
    "createdTime",
    "updatedTime"
];

/**
 * Class representing Selfie collection 
 * in mongoDB.
 */
export class Splits {
    _id: string = null;
    activityId: string = null;
    userId: string = null;
    distanceOrder: number = null;
    distance: number = null;
    timeTakenInSecs: number = null;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(dateFields.find(dateField => dateField === key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if (!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Activities_Splits";
}