import { MetadataHelper } from "@praveenspi/mysamay-common-util";

export class InactiveUser {
    _id: string = null;
    userId: string = null;
    userName: string = null;
    email: string = null;
    mobile: string = null;
    gender: string = null;
    day: number = null;
    month: number = null;
    year: number = null;
    date: Date = null;
    updatedTime: Date = null;
    lastActiveTime: Date = null;// initialSyncDate from activity


    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (key === "updatedTime" || key === "date") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Reports_InactiveUsers";
}