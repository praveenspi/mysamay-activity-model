import { MetadataHelper } from "@praveenspi/mysamay-common-util";

export class ActiveUser {
    _id: string = null;
    userId: string = null;
    userName: string = null;
    gender: string = null;
    day: number = null;
    month: number = null;
    year: number = null;
    activityCount: number = null;
    previousDistance: number = null;
    totalDistance: number = null;
    totalTime: number= null;
    avgPace: number = null;
    avgDistance: number = null;
    date: Date = null;
    updatedTime: Date = null;


    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (key === "updatedTime" || key === "date") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Reports_ActiveUsers";
}