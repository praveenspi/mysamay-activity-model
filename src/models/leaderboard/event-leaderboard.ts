import { MetadataHelper } from "@praveenspi/mysamay-common-util";

export class EventLeaderboard {
    _id: string = null;
     userId: string = null;
     rank: number = null;
     raceRank: string = null;
     activityCount: number = null;
     totalDistance: number = null;
     actualDistance: number = null;
     avgDistance: number = null;
     avgPace: number = null;
     type: string = null;
     userName: string = null;
     gender: string = null;
     ageGroup: string = null;
     genderRank: string = null;
     ageGroupRank: string = null;
     activeTime: number = null;
     consideredTime: number = null;
     clubId: string = null;
     corporateId: string = null;
     eventId: string = null;
     eventCategory: string = null;
     profilePicture: string = null;
     activitySource: string = null;
     updatedTime: Date = null;
     raceCategory: string = null;
    

    constructor(data: Partial<EventLeaderboard>) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(key === "updatedTime") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Events_LeaderBoard";
}