import { MetadataHelper } from "@praveenspi/mysamay-common-util";

export class CityLeaderboard {
    _id: string = null;
    city: string = null;
    rank: number = null;
    activityCount: number = null;
    totalDistance: number = null;
    totalParticipants: number = null;
    eventId: string = null;
    updatedTime: Date = null;

    constructor(data: Partial<CityLeaderboard>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (key === "updatedTime") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Events_CityLeaderboard";
}