import { MetadataHelper } from "@praveenspi/mysamay-common-util";

export class ActivityLeaderboard {
    _id: string = null;
     userId: string = null;
     activityCount: number = null;
     totalDistance: number = null;
     avgDistance: number = null;
     avgPace: number = null;
     type: string = null;
     userName: string = null;
     gender: string = null;
     updatedTime: Date = null;
    

    constructor(data: Partial<ActivityLeaderboard>) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(key === "updatedTime") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Activities_LeaderBoard";
}