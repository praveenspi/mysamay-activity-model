import { MetadataHelper } from "@praveenspi/mysamay-common-util";

export class StepsLeaderboard {
    _id: string = null;
     userId: string = null;
     rank: number = null;
     raceRank: string = null;
     totalStepCount: number = null;
     userName: string = null;
     gender: string = null;
     ageGroup: string = null;
     genderRank: string = null;
     ageGroupRank: string = null;
     eventId: string = null;
     eventCategory: string = null;
     profilePicture: string = null;
     previousDayStepCount: number = null;
     updatedTime: Date = null;
    
    constructor(data: Partial<StepsLeaderboard>) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(key === "updatedTime") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Event_StepsLeaderBoard";
}