import { MetadataHelper } from "@praveenspi/mysamay-common-util";

export class Geopoint {
    _id: string = null;
    activityId: string = null;
    /**
     * Latitude
     */
    lat: number = null;
    /**
     * Longitude
     */
    lon: number = null;
    /**
     * Altitude
     */
    alt: number = null;
    fluctuated: boolean = null;
    idle: boolean = null;
    /**
     * GPS accuracy
     */
    accuracy: number = null;
    /**
     * Distance covered since starting activity.
     */
    distance: number = null;
    /**
     * In vehicle, on foot etc.
     */
    type: string = null;
    pace: number = null;
    speed: number = null;
    heartRate: number = null;
    distanceFromSteps: number = null;
    typeAccuracy: number = null;
    /**
     * Seconds since starting activity.
     */
    duration: number;
    timeStamp: Date = null;

    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(key === "timeStamp") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Activities_GeoLocations";
}