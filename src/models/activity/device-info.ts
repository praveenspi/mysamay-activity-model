export class DeviceInfo {
    make: string;
    model: string;
    os: string;
    appVersion: string;
}