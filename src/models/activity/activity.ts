import { MetadataHelper } from "@praveenspi/mysamay-common-util";
import { DeviceInfo } from "./device-info";
import { Geopoint } from "./geopoint";

const dateFields = [
    "startTime",
    "endTime",
    "createdTime",
    "updatedTime",
    "initialSyncDate"
];

export class Activity {
    _id: string = null;
    /**
     * Type of activity.
     * Run | Walk
     */
     activityType: string = null;
     detectedActvityType: string = null;
     activityTitle: string = null;
     /**
      * Approved or Rejected.
      */
     activityStatus: string = null;
     /**
      * Reason for Rejection.
      */
     activityStatusReason: string = null;
     cheatingDetected: boolean = false;
     activityDescription: string = null;
     /**
      * In vehicle, on foot etc.
      */
     activityMode: string = null;
     userId: string = null;
     startTime: Date = new Date();
     endTime: Date = new Date();
     /**
      * Distance covered in KM
      */
     distance: number = null;
     /**
      * Total time in seconds.
      */
     elapsedTime: number = null;
     /**
      * Moving time in seconds.
      */
     activeTime: number = null;
     stepCount: number = null;
     avgPace: number = null;
     avgSpeed: number = null;
     avgCadence: number = null;
     avgHeartRate: number = null;
     calories: number = null;
     elevationGained: number = null;
     elevationLost: number = null;
     polyline: Geopoint[] = null;
     polylineStr: string = null;
     eventId: string = null;
     eventName: string = null;
     eventCategory: string = null;
     eventLogo: string = null;
     selfieCount: number = null;
     thumbnail: string = null;
     manual: boolean = false;
     imported: boolean = false;
     deviceInfo: DeviceInfo = null;
     createdTime: Date = null;
     updatedTime: Date = null;
     date: string = null;
     activitySource: string = "MySamay";
     externalActivityId: string = null;
     /**
      * Name of the device on which activity was recorded.
      */
     sourceName: string = null;
     initialSyncDate: Date = null;

    constructor(data: Partial<Activity>) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(key === "polyline") {
                    this[key] = data[key].map((p: any) => {
                        return new Geopoint(p);
                    })
                }
                else if(dateFields.find(dateField => dateField === key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.createdTime) {
            this.createdTime = this.startTime;
        }
        this.updatedTime = this.startTime;
    }

    static collectionName = "Activities_Activity";
}