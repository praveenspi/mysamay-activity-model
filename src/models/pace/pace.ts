import { MetadataHelper } from "@praveenspi/mysamay-common-util";

export class Pace {
    _id: string = null;
    activityId: string = null;
    distance: number = null;
    distanceFromSteps: number = null;
    pace: number = null;
    timeStamp: Date = null;

    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(key === "timeStamp") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Activities_Pace";
}